﻿using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.Task;

namespace ProjectStructure.Common.DTO.ResultModels
{
	public class ProjectTasksTeamInfoDTO
	{
		public ProjectDTO Project { get; set; }
		public TaskDTO LongestDescriptionTask { get; set; }
		public TaskDTO ShortestNameTask { get; set; }
		public int TeamMembersCount { get; set; }
	}
}
