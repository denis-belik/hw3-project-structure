﻿using AutoMapper;
using ProjectStructure.BLL.Abstractions.Queries;
using ProjectStructure.BLL.Abstractions.Queries.Handlers;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Queries.Teams;
using ProjectStructure.Common.DTO.ResultModels;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.Common.Helpers;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Queries.Handlers
{
	public class TeamsQueryHandler : QueryHandler<TeamDTO>
	{
		public TeamsQueryHandler(FakeDbContext context, IMapper mapper)
			: base(context, mapper)
		{ }

		public override TeamDTO Handle(GetByIdQuery query)
		{
			var team = _context.Teams.FirstOrDefault(team => team.Id == query.EntityId);

			if (team == null)
			{
				throw new NotFoundException(nameof(Team), query.EntityId);
			}

			return _mapper.Map<TeamDTO>(team);
		}

		public override IEnumerable<TeamDTO> Handle(GetAllQuery query)
		{
			var teams = JoinHelper.TeamJoins(
					_context.Teams,
					_context.Users);

			return _mapper.Map<ICollection<TeamDTO>>(teams);
		}

		// 4. Отримати список (id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 10 років,
		// відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.
		public IEnumerable<TeamMembersDTO> Handle(GetTeamMembersByAgeQuery query)
		{
			return _context.Users
				.Join(
					_context.Teams,
					user => user.TeamId,
					team => team.Id,
					(user, team) => new User(user, team))
				.Where(user => DateTime.Now.Year - user.Birthday.Year > query.MinAge)
				.OrderByDescending(user => user.RegisteredAt)
				.GroupBy(
					user => user.Team,
					(team, users) => new TeamMembersDTO
					{
						Id = team.Id,
						Name = team.Name,
						Members = _mapper.Map<ICollection<UserDTO>>(users)
					});
		}
	}
}
