﻿using Microsoft.AspNetCore.Mvc;
using ProjectStructure.BLL.Abstractions.Queries;
using ProjectStructure.BLL.Commands.Handlers;
using ProjectStructure.BLL.Commands.Users;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Queries.Handlers;
using ProjectStructure.BLL.Queries.Users;
using ProjectStructure.Common.DTO.ResultModels;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class UsersController : ControllerBase
	{
		UsersQueryHandler _queryHandler;
		UsersCommandHandler _commandHandler;

		public UsersController(UsersQueryHandler queryHandler, UsersCommandHandler commandHandler)
		{
			_queryHandler = queryHandler;
			_commandHandler = commandHandler;
		}

		[HttpGet]
		public IActionResult Get()
		{
			try
			{
				var usersDto = _queryHandler.Handle(new GetAllQuery());
				return Ok(usersDto);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}		
		}

		[HttpGet("sorted/withTasks")]
		public IActionResult GetSortedUsersWithTasks()
		{
			try
			{
				var usersDto = _queryHandler.Handle(new GetSortedUsersWithTasksQuery());
				return Ok(usersDto);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}	
		}

		[HttpGet("{id}")]
		public IActionResult Get(int id)
		{
			try
			{
				var userDto = _queryHandler.Handle(new GetByIdQuery(id));
				return Ok(userDto);
			}
			catch(NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}	
		}

		[HttpGet("{id}/projectTasksInfo")]
		public IActionResult GetUserProjectTasksInfo(int id)
		{
			try
			{
				var usersDto = _queryHandler.Handle(new GetUserProjectTasksInfoQuery() { UserId = id });
				return Ok(usersDto);
			}
			catch (NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}		
		}

		[HttpGet("{id}/ProjectTasksCount")]
		public IActionResult GetProjectTasksCount(int id)
		{
			try
			{
				var dictionary = _queryHandler.Handle(new GetProjectTasksCountQuery { ProjectAuthorId = id });
				var fakeDictionary = dictionary.Select(keyValue => new FakeProjectTasksCountDictionaryDTO 
				{ 
					Project = keyValue.Key, 
					TasksCount = keyValue.Value 
				});
				return Ok(fakeDictionary);
			}
			catch (NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}
		}

		[HttpPost]
		public IActionResult Post([FromBody] UserCreateDTO userDto)
		{
			try
			{
				int createdId = _commandHandler.Handle(new AddUserCommand { UserCreateDto = userDto });
				return Created($"api/users/{createdId}", userDto);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}	
		}

		[HttpPut]
		public IActionResult Put([FromBody] UserUpdateDTO userUpdateDto)
		{
			try
			{
				_commandHandler.Handle(new UpdateUserCommand { UserUpdateDto = userUpdateDto });
				return NoContent();
			}
			catch (NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}
		}

		[HttpDelete("{id}")]
		public IActionResult Delete(int id)
		{
			try
			{
				_commandHandler.Handle(new DeleteUserCommand { UserId = id });
				return NoContent();
			}
			catch (NotFoundException ex)
			{
				return NotFound(ex.Message);
			}
			catch (Exception ex)
			{
				return StatusCode(500, ex.Message);
			}	
		}
	}
}
