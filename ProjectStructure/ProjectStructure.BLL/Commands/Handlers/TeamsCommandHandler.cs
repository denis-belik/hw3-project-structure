﻿using AutoMapper;
using ProjectStructure.BLL.Abstractions.Commands;
using ProjectStructure.BLL.Abstractions.Commands.Handlers;
using ProjectStructure.BLL.Commands.Teams;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.Common.DTO.Team;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;
using System.Linq;

namespace ProjectStructure.BLL.Commands.Handlers
{
	public class TeamsCommandHandler : CommandHandler
	{
		public TeamsCommandHandler(FakeDbContext context, IMapper mapper)
			: base(context, mapper)
		{ }

		public override int Handle(AddCommand addCommand)
		{
			AddTeamCommand command = addCommand as AddTeamCommand;
			TeamCreateDTO teamCreate = command.TeamCreateDto;

			Team team = _mapper.Map<Team>(teamCreate);

			int lastTeamId = _context.Teams.OrderBy(team => team.Id).LastOrDefault().Id;
			team.Id = lastTeamId + 1;

			_context.Teams.Add(team);

			return team.Id;
		}

		public override void Handle(DeleteCommand deleteCommand)
		{
			DeleteTeamCommand command = deleteCommand as DeleteTeamCommand;

			Team team = _context.Teams.FirstOrDefault(team => team.Id == command.TeamId);

			if (team == null)
			{
				throw new NotFoundException(nameof(Team), command.TeamId);
			}

			_context.Teams.Remove(team);
		}

		public override void Handle(UpdateCommand updateCommand)
		{
			UpdateTeamCommand command = updateCommand as UpdateTeamCommand;
			TeamUpdateDTO teamUpdate = command.TeamUpdateDto;

			Team team = _context.Teams.FirstOrDefault(team => team.Id == teamUpdate.Id);

			if (team == null)
			{
				throw new NotFoundException(nameof(Team), teamUpdate.Id);
			}

			team.Name = string.IsNullOrEmpty(teamUpdate.Name?.Trim())
					  ? team.Name
					  : teamUpdate.Name;
		}
	}
}
