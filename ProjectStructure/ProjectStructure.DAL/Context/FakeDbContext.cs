﻿using Newtonsoft.Json;
using ProjectStructure.DAL.Models;
using System.Collections.Generic;
using System.IO;

namespace ProjectStructure.DAL.Context
{
	public class FakeDbContext
	{
		public FakeDbContext()
		{
			string path =
					Directory.GetParent(
						Directory.GetCurrentDirectory()).FullName + @"\ProjectStructure.DAL\Json";

			Users = DeserializeCollection<User>(path, "users");
			Projects = DeserializeCollection<Project>(path, "projects");
			Tasks = DeserializeCollection<Task>(path, "tasks");
			Teams = DeserializeCollection<Team>(path, "teams");
		}

		public List<T> DeserializeCollection<T>(string path, string fileName)
		{
			List<T> collection = null;

			using (StreamReader reader = new StreamReader($@"{path}\{fileName}.json"))
			{
				string json = reader.ReadToEnd();
				collection = JsonConvert.DeserializeObject<List<T>>(json);
			}

			return collection;
		}

		public List<User> Users { get; set; }

		public List<Project> Projects { get; set; }

		public List<Task> Tasks { get; set; }

		public List<Team> Teams { get; set; }
	}
}
