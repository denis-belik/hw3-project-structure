﻿using AutoMapper;
using ProjectStructure.BLL.Abstractions.Queries;
using ProjectStructure.BLL.Abstractions.Queries.Handlers;
using ProjectStructure.DAL.Context;
using System.Collections.Generic;
using System.Linq;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.DAL.Models;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.ResultModels;
using ProjectStructure.BLL.Queries.Projects;
using ProjectStructure.Common.DTO.Task;

namespace ProjectStructure.BLL.Queries.Handlers
{
	public class ProjectsQueryHandler : QueryHandler<ProjectDTO>
	{
		public ProjectsQueryHandler(FakeDbContext context, IMapper mapper)
			: base(context, mapper)
		{ }

		public override ProjectDTO Handle(GetByIdQuery query)
		{
			var project = _context.Projects.FirstOrDefault(project => project.Id == query.EntityId);

			if (project == null)
			{
				throw new NotFoundException(nameof(Project), query.EntityId);
			}

			return _mapper.Map<ProjectDTO>(project);
		}

		public override IEnumerable<ProjectDTO> Handle(GetAllQuery _)
		{
			var projects = _context.Projects;
			return _mapper.Map<ICollection<ProjectDTO>>(projects);
		}

		// 7. Отримати таку структуру:
		// Проект
		// Найдовший таск проекту(за описом)
		// Найкоротший таск проекту(по імені)
		// Загальна кількість користувачів в команді проекту, де або опис проекту >20 символів, або кількість тасків <3
		public IEnumerable<ProjectTasksTeamInfoDTO> Handle(GetProjectTasksTeamInfosQuery query)
		{
			return
				_context.Projects
				.GroupJoin(
					_context.Tasks,
					project => project.Id,
					task => task.ProjectId,
					(project, taskGroup) => new Project(project, taskGroup))
				.Join(
					_context.Teams.GroupJoin(
						_context.Users,
						team => team.Id,
						user => user.TeamId,
						(team, userGroup) => new Team(team, userGroup)),
					project => project.TeamId,
					team => team.Id,
					(project, team) => new Project(project, team) { Tasks = project.Tasks })
				.Select(project => new ProjectTasksTeamInfoDTO
				{
					Project = _mapper.Map<ProjectDTO>(project),

					LongestDescriptionTask = _mapper.Map<TaskDTO>(
						project.Tasks.FirstOrDefault(
										task => task.Description?.Length ==
												project.Tasks.Max(task => task.Description?.Length))),

					ShortestNameTask = _mapper.Map<TaskDTO>(
						project.Tasks.FirstOrDefault(
										task => task.Name?.Length ==
												project.Tasks.Min(task => task.Name?.Length))),

					TeamMembersCount =
						project.Description.Length > query.MinDescriptionLength || project.Tasks.Count() < query.MaxTaskCount ?
						project.Team.Members.Count() :
						0
				});
		}
	}
}
