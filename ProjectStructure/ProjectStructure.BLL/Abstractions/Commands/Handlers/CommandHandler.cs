﻿using AutoMapper;
using ProjectStructure.BLL.Commands.Users;
using ProjectStructure.DAL.Context;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProjectStructure.BLL.Abstractions.Commands.Handlers
{
	public abstract class CommandHandler
	{
		protected readonly FakeDbContext _context;
		protected readonly IMapper _mapper;

		public CommandHandler(FakeDbContext context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
		}

		public abstract int Handle(AddCommand command);
		public abstract void Handle(DeleteCommand command);
		public abstract void Handle(UpdateCommand command);
	}
}
