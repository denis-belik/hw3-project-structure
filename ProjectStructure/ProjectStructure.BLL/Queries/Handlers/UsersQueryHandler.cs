﻿using AutoMapper;
using ProjectStructure.BLL.Abstractions.Queries;
using ProjectStructure.BLL.Abstractions.Queries.Handlers;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Queries.Users;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.ResultModels;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.User;
using ProjectStructure.Common.Helpers;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.BLL.Queries.Handlers
{
	public class UsersQueryHandler : QueryHandler<UserDTO>
	{
		public UsersQueryHandler(FakeDbContext context, IMapper mapper)
			: base(context, mapper)
		{ }
		
		public override UserDTO Handle(GetByIdQuery query)
		{
			var user = _context.Users.FirstOrDefault(user => user.Id == query.EntityId);

			if (user == null)
			{
				throw new NotFoundException(nameof(User), query.EntityId); 
			}

			return _mapper.Map<UserDTO>(user);
		}

		public override IEnumerable<UserDTO> Handle(GetAllQuery _)
		{
			var users = _context.Users;
			return _mapper.Map<ICollection<UserDTO>>(users);
		}

		// 1. Отримати кількість тасків у проекті конкретного користувача(по id) (словник, де key буде проект, а value кількість тасків).
		public Dictionary<ProjectDTO, int> Handle(GetProjectTasksCountQuery query)
		{
			if (!_context.Users.Any(user => user.Id == query.ProjectAuthorId))
			{
				throw new NotFoundException(nameof(User), query.ProjectAuthorId);
			}

			return _context.Projects
				.Where(project => project.AuthorId == query.ProjectAuthorId)
				.GroupJoin(
					_context.Tasks,
					project => project.Id,
					task => task.ProjectId,
					(project, taskGroup) => new Project(project, taskGroup))
				.ToDictionary(project =>
					_mapper.Map<ProjectDTO>(project),
					project => project.Tasks.Count());
		}

		// 5. Отримати список користувачів за алфавітом first_name(по зростанню) з відсортованими tasks по довжині name(за спаданням).
		public IEnumerable<UserWithTasksDTO> Handle(GetSortedUsersWithTasksQuery _)
		{
			var users = JoinHelper.UserJoins(
				users: _context.Users.OrderBy(user => user.FirstName),
				tasks: _context.Tasks.OrderByDescending(task => task.Name.Length));

			return _mapper.Map<ICollection<UserWithTasksDTO>>(users);
		}


		// 6. Отримати наступну структуру (передати Id користувача в параметри):
		// User
		// Останній проект користувача (за датою створення)
		// Загальна кількість тасків під останнім проектом
		// Загальна кількість незавершених або скасованих тасків для користувача
		// Найтриваліший таск користувача за датою (найраніше створений - найпізніше закінчений)
		public UserProjectTasksInfoDTO Handle(GetUserProjectTasksInfoQuery query)
		{
			var user = _context.Users.FirstOrDefault(user => user.Id == query.UserId);
			if(!_context.Users.Any(user => user.Id == query.UserId))
			{
				throw new NotFoundException(nameof(User), query.UserId);
			}

			return new List<User> { _context.Users.FirstOrDefault(user => user.Id == query.UserId) }
				.GroupJoin(
					_context.Projects,
					user => user.Id,
					project => project.AuthorId,
					(user, projectGroup) =>
						new User(user, projectGroup
										.OrderByDescending(project => project.CreatedAt)
										.GroupJoin(
											_context.Tasks,
											project => project.Id,
											task => task.ProjectId,
											(project, taskGroup) => new Project(project, taskGroup))))
				.GroupJoin(
					_context.Tasks,
					user => user.Id,
					task => task.PerformerId,
					(user, taskGroup) =>
						new User(user, taskGroup) { Projects = user.Projects })
				.Select(user => new UserProjectTasksInfoDTO
				{
					User = _mapper.Map<UserDTO>(user),

					LatestProject = _mapper.Map<ProjectDTO>(user.Projects.FirstOrDefault()),

					LatestProjectTasksCount = user.Projects.FirstOrDefault()?.Tasks.Count(),

					UsersNotFinishedTasksCount = user.Tasks.Count(task => task.State != TaskState.Finished),

					UsersLongestDurationTask = _mapper.Map<TaskDTO>(
						user.Tasks
						.OrderByDescending(task => task.FinishedAt - task.CreatedAt)
						.FirstOrDefault())
				})
				.FirstOrDefault();
		}

	}
}
