﻿using System;
using System.Collections.Generic;

namespace ProjectStructure.DAL.Models
{
	public class Project
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime Deadline { get; set; }
		public int AuthorId { get; set; }
		public int TeamId { get; set; }

		public User Author { get; set; }
		public Team Team { get; set; }
		public IEnumerable<Task> Tasks { get; set; }

		public Project() { }
		public Project(Project project)
		{
			Id = project.Id;
			Name = project.Name;
			Description = project.Description;
			CreatedAt = project.CreatedAt;
			Deadline = project.Deadline;
			AuthorId = project.AuthorId;
			TeamId = project.TeamId;

			Tasks = project.Tasks;
			Team = project.Team;
			Author = project.Author;
		}

		public Project(Project project, IEnumerable<Task> tasks) : this(project)
		{
			Tasks = tasks;
		}

		public Project(Project project, Team team) : this(project) 
		{
			Team = team;
		}

		public Project(Project project, User author) : this(project)
		{
			Author = author;
		}
	}
}
