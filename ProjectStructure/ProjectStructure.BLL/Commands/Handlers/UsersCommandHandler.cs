﻿using ProjectStructure.BLL.Commands.Users;
using ProjectStructure.DAL.Context;
using System.Linq;
using AutoMapper;
using ProjectStructure.DAL.Models;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.BLL.Abstractions.Commands.Handlers;
using ProjectStructure.BLL.Abstractions.Commands;

namespace ProjectStructure.BLL.Commands.Handlers
{
	public class UsersCommandHandler : CommandHandler
	{
		public UsersCommandHandler(FakeDbContext context, IMapper mapper)
			: base(context, mapper)
		{ }
			
		public override int Handle(AddCommand addCommand)
		{
			AddUserCommand command = addCommand as AddUserCommand;

			User user = _mapper.Map<User>(command.UserCreateDto);

			int lastUserId = _context.Users.OrderBy(user => user.Id).LastOrDefault().Id;
			user.Id = lastUserId + 1;

			_context.Users.Add(user);

			return user.Id;
		}

		public override void Handle(DeleteCommand deleteCommand)
		{
			DeleteUserCommand command = deleteCommand as DeleteUserCommand;

			User user = _context.Users.FirstOrDefault(user => user.Id == command.UserId);

			if(user == null)
			{
				throw new NotFoundException(nameof(User), command.UserId);
			}

			// remove connected entities
			_context.Projects.RemoveAll(project => project.AuthorId == user.Id);
			_context.Tasks.RemoveAll(task => task.PerformerId == user.Id);

			_context.Users.Remove(user);
		}

		public override void Handle(UpdateCommand updateCommand)
		{
			UpdateUserCommand command = updateCommand as UpdateUserCommand;
			var userUpdate = command.UserUpdateDto;

			User userEntity = _context.Users.FirstOrDefault(user => user.Id == userUpdate.Id);

			if (userEntity == null)
			{
				throw new NotFoundException(nameof(User), command.UserUpdateDto.Id);
			}
			
			userEntity.FirstName = string.IsNullOrEmpty(userUpdate.FirstName?.Trim()) 
								 ? userEntity.FirstName
								 : userUpdate.FirstName;

			userEntity.LastName = string.IsNullOrEmpty(userUpdate.LastName?.Trim()) 
								? userEntity.LastName
								: userUpdate.LastName;

			userEntity.Email = string.IsNullOrEmpty(userUpdate.Email?.Trim()) 
							 ? userEntity.Email
							 : userUpdate.Email;
			
			if(userUpdate.TeamId == null) // kick user from team
			{
				userEntity.TeamId = userUpdate.TeamId;
			}
			else
			{
				if(command.UserUpdateDto.TeamId > -1)
				{
					if (!_context.Teams.Any(team => team.Id == command.UserUpdateDto.TeamId))
					{
						throw new NotFoundException(nameof(Team), (int)command.UserUpdateDto.TeamId);
					}
					userEntity.TeamId = command.UserUpdateDto.TeamId;
				}
			}
		}
	}
}
