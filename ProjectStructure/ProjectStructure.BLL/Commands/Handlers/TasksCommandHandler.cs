﻿using AutoMapper;
using ProjectStructure.BLL.Abstractions.Commands;
using ProjectStructure.BLL.Abstractions.Commands.Handlers;
using ProjectStructure.BLL.Commands.Tasks;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructure.BLL.Commands.Handlers
{
	public class TasksCommandHandler : CommandHandler
	{
		public TasksCommandHandler(FakeDbContext context, IMapper mapper)
			: base(context, mapper)
		{ }

		public override int Handle(AddCommand addCommand)
		{
			AddTaskCommand command = addCommand as AddTaskCommand;
			TaskCreateDTO taskCreate = command.TaskCreateDto;

			if (!_context.Users.Any(user => user.Id == taskCreate.PerformerId))
			{
				throw new NotFoundException(nameof(User), taskCreate.PerformerId);
			}

			if (!_context.Projects.Any(project => project.Id == taskCreate.ProjectId))
			{
				throw new NotFoundException(nameof(Team), taskCreate.ProjectId);
			}

			Task task = _mapper.Map<Task>(taskCreate);

			int lastTaskId = _context.Tasks.OrderBy(task => task.Id).LastOrDefault().Id;
			task.Id = lastTaskId + 1;

			_context.Tasks.Add(task);

			return task.Id;
		}

		public override void Handle(DeleteCommand deleteCommand)
		{
			DeleteTaskCommand command = deleteCommand as DeleteTaskCommand;

			Task task = _context.Tasks.FirstOrDefault(task => task.Id == command.TaskId);

			if (task == null)
			{
				throw new NotFoundException(nameof(Task), command.TaskId);
			}

			_context.Tasks.Remove(task);
		}

		public override void Handle(UpdateCommand updateCommand)
		{
			UpdateTaskCommand command = updateCommand as UpdateTaskCommand;
			TaskUpdateDTO taskUpdate = command.TaskUpdateDto;

			Task taskEntity = _context.Tasks.FirstOrDefault(task => task.Id == taskUpdate.Id);

			if (taskEntity == null)
			{
				throw new NotFoundException(nameof(Task), taskUpdate.Id);
			}

			taskEntity.Name = string.IsNullOrEmpty(taskUpdate.Name?.Trim())
				? taskEntity.Name
				: taskUpdate.Name;

			taskEntity.Description = string.IsNullOrEmpty(taskUpdate.Description?.Trim())
				? taskEntity.Description
				: taskUpdate.Description;

			taskEntity.FinishedAt = taskUpdate.FinishedAt ?? taskEntity.FinishedAt;

			if(taskUpdate.State != null)
			{
				if(taskUpdate.State >= 0 && taskUpdate.State <= TaskState.Canceled )
				{
					taskEntity.State = (TaskState)taskUpdate.State;
				}
			}
			
			if(taskUpdate.PerformerId != null)
			{
				if(!_context.Users.Any(user => user.Id == taskUpdate.PerformerId))
				{
					throw new NotFoundException(nameof(User), (int)taskUpdate.PerformerId);
				}
				taskEntity.PerformerId = (int)taskUpdate.PerformerId;
			}
		}
	}
}
