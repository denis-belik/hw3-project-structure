﻿using ProjectStructure.DAL.Models;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.Common.Helpers
{
	public static class JoinHelper
	{
		public static IEnumerable<User> UserJoins(
			IEnumerable<User> users,
			IEnumerable<Team> teams = null,
			IEnumerable<Task> tasks = null,
			IEnumerable<Project> projects = null)
		{
			IEnumerable<User> joinedUsers = users;

			if (teams != null)
			{
				joinedUsers = users
					.GroupJoin(
						teams,
						user => user.TeamId,
						team => team.Id,
						(user, team) => new User(user, team.FirstOrDefault()));
			}

			if (tasks != null)
			{
				joinedUsers = joinedUsers
					.GroupJoin(
						tasks,
						user => user.Id,
						task => task.PerformerId,
						(user, taskGroup) => new User(user, taskGroup));
			}

			if (projects != null)
			{
				joinedUsers = joinedUsers
					.GroupJoin(
						projects,
						user => user.Id,
						project => project.AuthorId,
						(user, projectGroup) => new User(user, projectGroup));
			}

			return joinedUsers;
		}

		public static IEnumerable<Project> ProjectJoins(
			IEnumerable<Project> projects,
			IEnumerable<User> authors = null,
			IEnumerable<Team> teams = null,
			IEnumerable<Task> tasks = null)
		{
			IEnumerable<Project> joinedProjects = projects;

			if (authors != null)
			{
				joinedProjects = joinedProjects
					.GroupJoin(
						authors,
						project => project.AuthorId,
						author => author.Id,
						(project, author) => new Project(project, author.FirstOrDefault()));
			}

			if (teams != null)
			{
				joinedProjects = joinedProjects
					.GroupJoin(
						teams,
						project => project.TeamId,
						team => team.Id,
						(project, team) => new Project(project, team.FirstOrDefault()));
			}

			if (tasks != null)
			{
				joinedProjects = joinedProjects
					.GroupJoin(
						tasks,
						project => project.Id,
						task => task.ProjectId,
						(project, taskGroup) => new Project(project, taskGroup));
			}

			return joinedProjects;
		}

		public static IEnumerable<Task> TaskJoins(
			IEnumerable<Task> tasks,
			IEnumerable<User> users,
			IEnumerable<Project> projects)
		{
			IEnumerable<Task> joinedTasks = tasks;

			if(users != null)
			{
				joinedTasks = joinedTasks
					.GroupJoin(
						users,
						task => task.PerformerId,
						user => user.Id,
						(task, user) => new Task(task, user.FirstOrDefault()));
			}

			if(projects != null)
			{
				joinedTasks = joinedTasks
					.GroupJoin(
						projects,
						task => task.ProjectId,
						project => project.Id,
						(task, project) => new Task(task, project.FirstOrDefault()));
			}

			return joinedTasks;
		}

		public static IEnumerable<Team> TeamJoins(
			IEnumerable<Team> teams,
			IEnumerable<User> users)
		{
			var joinedTeams = teams;
			if(users != null)
			{
				joinedTeams = joinedTeams
					.GroupJoin(
						users,
						team => team.Id,
						user => user.TeamId,
						(team, userGroup) => new Team(team, userGroup));
			}

			return joinedTeams;
		}
	}
}
