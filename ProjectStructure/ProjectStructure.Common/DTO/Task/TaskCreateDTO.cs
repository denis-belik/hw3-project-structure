﻿using System;

namespace ProjectStructure.Common.DTO.Task
{
	public class TaskCreateDTO
	{
		public string Name { get; set; }
		public string Description { get; set; }
		public DateTime CreatedAt { get; set; } = DateTime.Now;
		public DateTime FinishedAt { get; set; }

		public int ProjectId { get; set; }
		public int PerformerId { get; set; }
	}
}
