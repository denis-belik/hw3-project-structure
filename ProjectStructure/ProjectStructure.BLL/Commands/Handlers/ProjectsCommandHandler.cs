﻿using AutoMapper;
using ProjectStructure.BLL.Abstractions.Commands;
using ProjectStructure.BLL.Abstractions.Commands.Handlers;
using ProjectStructure.BLL.Commands.Projects;
using ProjectStructure.BLL.Exceptions;
using ProjectStructure.Common.DTO.Project;
using ProjectStructure.DAL.Context;
using ProjectStructure.DAL.Models;
using System;
using System.Linq;

namespace ProjectStructure.BLL.Commands.Handlers
{
	public class ProjectsCommandHandler : CommandHandler
	{
		public ProjectsCommandHandler(FakeDbContext context, IMapper mapper)
			: base(context, mapper)
		{ }

		public override int Handle(AddCommand addCommand)
		{
			AddProjectCommand command = addCommand as AddProjectCommand;
			ProjectCreateDTO projectCreate = command.ProjectCreateDto;

			if(!_context.Users.Any(user => user.Id == projectCreate.AuthorId))
			{
				throw new NotFoundException(nameof(User), projectCreate.AuthorId);
			}

			if(!_context.Teams.Any(task => task.Id == projectCreate.TeamId))
			{
				throw new NotFoundException(nameof(Team), projectCreate.TeamId);
			}

			Project project = _mapper.Map<Project>(projectCreate);

			int lastProjectId = _context.Projects.OrderBy(project => project.Id).LastOrDefault().Id;
			project.Id = lastProjectId + 1;

			_context.Projects.Add(project);

			return project.Id;
		}

		public override void Handle(DeleteCommand deleteCommand)
		{
			DeleteProjectCommand command = deleteCommand as DeleteProjectCommand;

			Project project = _context.Projects.FirstOrDefault(project => project.Id == command.ProjectId);

			if (project == null)
			{
				throw new NotFoundException(nameof(Project), command.ProjectId);
			}

			// remove connected entities
			_context.Tasks.RemoveAll(task => task.ProjectId == project.Id);

			_context.Projects.Remove(project);
		}

		public override void Handle(UpdateCommand updateCommand)
		{
			UpdateProjectCommand command = updateCommand as UpdateProjectCommand;
			ProjectUpdateDTO projectUpdate = command.ProjectUpdateDto;

			Project projectEntity = _context.Projects.FirstOrDefault(project => project.Id == projectUpdate.Id);

			if (projectEntity == null)
			{
				throw new NotFoundException(nameof(Project), projectUpdate.Id);
			}

			projectEntity.Name = string.IsNullOrEmpty(projectUpdate.Name?.Trim()) 
							   ? projectEntity.Name 
							   : projectUpdate.Name;

			projectEntity.Description = string.IsNullOrEmpty(projectUpdate.Description?.Trim())
									  ? projectEntity.Description
									  : projectUpdate.Description;

			if(projectUpdate.Deadline > DateTime.Now)
			{
				projectEntity.Deadline = projectUpdate.Deadline;
			}

			if(projectUpdate.TeamId != null)
			{
				if (!_context.Teams.Any(team => team.Id == projectEntity.TeamId))
				{
					throw new NotFoundException(nameof(Team), (int)projectUpdate.TeamId);
				}
				projectEntity.TeamId = (int)projectUpdate.TeamId;
			}
		}
	}
}
