﻿using ProjectStructure.Common.DTO.Project;
using ProjectStructure.Common.DTO.Task;
using ProjectStructure.Common.DTO.User;

namespace ProjectStructure.Common.DTO.ResultModels
{
	public class UserProjectTasksInfoDTO
	{
		public UserDTO User { get; set; }
		public ProjectDTO LatestProject { get; set; }	
		public int? LatestProjectTasksCount { get; set; }	
		public int UsersNotFinishedTasksCount { get; set; }
		public TaskDTO UsersLongestDurationTask { get; set; }
	}
}
